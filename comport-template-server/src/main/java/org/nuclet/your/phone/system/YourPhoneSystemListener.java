package org.nuclet.your.phone.system;

/**
 * 
 * Just one example an interface could be. Ask your manufacture for a java interface.
 */
public interface YourPhoneSystemListener {

	void callChanged(YourPhoneSystemCall changedCall);

}
