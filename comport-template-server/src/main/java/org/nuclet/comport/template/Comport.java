package org.nuclet.comport.template;

import java.net.SocketException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuclet.your.phone.system.YourPhoneSystemCall;
import org.nuclet.your.phone.system.YourPhoneSystemListener;
import org.nuclet.your.phone.system.YourPhoneSystemPeer;
import org.nuclet.your.phone.system.YourPhoneSystemState;
import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.common.NuclosUserCommunicationAccount;
import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.communication.response.PhoneCallResponse;
import org.nuclos.api.communication.response.PhoneCallResponse.State;
import org.nuclos.api.context.communication.PhoneCallNotificationContext;
import org.nuclos.api.context.communication.PhoneCallRequestContext;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.CommunicationProvider;
import org.nuclos.api.provider.QueryProvider;

public class Comport implements CommunicationPort, Runnable, YourPhoneSystemListener {
	
	Logger LOG = LogManager.getLogger(Comport.class);
	
	private UID portId;		
	private String sHost;
	private String sPort;
	
	private final static String NULL = "NULL";
	private final ConcurrentMap<String, String> mapAccountsNull = new ConcurrentHashMap<String, String>();
	private final ConcurrentMap<String, NuclosUserCommunicationAccount> mapAccounts = new ConcurrentHashMap<String, NuclosUserCommunicationAccount>();
	private final ConcurrentMap<String, RequestedCall> mapRequestCall = new ConcurrentHashMap<String, RequestedCall>();
	
	private boolean kill = false;
	
	public void setId(UID portId) {
		this.portId = portId;
	}
	
	public void setHost(String sHost) {
		this.sHost = sHost;
	}

	public void setPort(String sPort) {
		this.sPort = sPort;
	}
	
	@Override
	public UID getId() {
		return portId;
	}
	
	@Override
	public <C extends RequestContext<?>> void handleRequest(C context) throws BusinessException {
		PhoneCallRequestContext phoneCallContext = (PhoneCallRequestContext) context;
		LOG.info("User " + phoneCallContext.getNuclosUserAccount().getAccount() + " wants to call " + phoneCallContext.getToNumber());
		
		String agent = phoneCallContext.getNuclosUserAccount().getAccount();
		String service = phoneCallContext.getNuclosUserAccount().getCustom1();
		String destination = phoneCallContext.getToNumber();
		int surveyTime = 10;
		boolean callRec = false;
		boolean bClir = false;
		String sDisplay = null;
		
		try {
			RequestedCall reqCall = new RequestedCall(phoneCallContext);
			YourPhoneSystemCall ctiCall = YourPhoneSystemPeer.makeCall(agent, service, destination, surveyTime, callRec, bClir, sDisplay);
			String callId = ctiCall.getCallId();
			mapRequestCall.put(callId, reqCall);
		} catch (SocketException e) {
			LOG.error(e.getMessage(), e);
			throw new BusinessException(e.getMessage());
		}
	}
	
	public void shutdown() {
		LOG.info("Shutdown received");
		kill = true;
		disconnect();
	}
	
	private void disconnect() {
		LOG.info("Try to disconnect now... bye bye");
		try {
			YourPhoneSystemPeer.disconnect();
		} catch (SocketException e) {
			LOG.warn(e.getMessage());
		}
		LOG.info("Clearing caches");
		mapAccounts.clear();
		mapRequestCall.clear();
		LOG.info("Disconnect complete");
	}

	@Override
	public void run() {
		LOG.info("Connecting to " + sHost + ":" + sPort);
		try {
			YourPhoneSystemPeer.connect(sHost, new Integer(sPort));
		} catch (NumberFormatException | SocketException e) {
			LOG.error(e.getMessage(), e);
			return;
		}
		
		LOG.info("Connected");
		registerListener();
		
		long seconds = 0;
		
		while (!kill) {
			seconds++;
			if (seconds % 3600 == 0) {
				// every h
				clearOneDayOldRequestedCalls();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				LOG.error(e.getMessage(), e);
				kill = true;
				disconnect();
			}
		}
	}
	
	private void clearOneDayOldRequestedCalls() {
		// may be not properly ended...
		
		long oneDayInSeconds = 60 * 60 *24;
		long currentTimeInSeconds = System.currentTimeMillis() / 1000;

		try {
			Iterator<Entry<String, RequestedCall>> it = mapRequestCall.entrySet().iterator();
			while (it.hasNext()) {
				try {
					Entry<String, RequestedCall> call = it.next();
					if (call.getValue().requestedAt + oneDayInSeconds < currentTimeInSeconds) {
						LOG.info("Remove one day old call request from cache. CallId=" + call.getKey());
						it.remove();
					}
				} catch (NoSuchElementException e1) {
					// ignore ... multithreading
				}
			}
		} catch (Exception ex) {
			LOG.error("Removing of ond day old call requests failed: " + ex.getMessage(), ex);
		}
	}
	
	private void registerListener() {
		LOG.info("Register Listener");
		YourPhoneSystemPeer.registerListener(this);
	}
		
	
	/**
	 * Something your phone system driver wants to be implemented
	 * @param changedCall
	 */
	@Override
	public void callChanged(YourPhoneSystemCall changedCall) {
		if (!changedCall.isIntern()) {
			String agent = changedCall.getAgent();
			String callId = changedCall.getCallId();
			String partner = changedCall.getPartner();
			String service = changedCall.getService();
			YourPhoneSystemState state = changedCall.getState();
			LOG.info("YourPhoneSystemCall.callChanged, incoming=" + changedCall.isIncoming() + " agent=" + agent + ", callId=" + callId + ", partner=" + partner + ", service=" + service + ", state=" + state);
			
			if (partner.startsWith("0")) {
				partner = partner.substring(1);
			}
			String from = changedCall.isIncoming() ? partner : agent;
			String to = changedCall.isIncoming() ? agent : partner;
			
			if (state == YourPhoneSystemState.ALERTING) {
				sendToNuclos(callId, from, getNuclosAccountIfAny(from), service, to, getNuclosAccountIfAny(to), PhoneCallNotificationContext.State.RINGING);
			}
			if (state == YourPhoneSystemState.CONNECTED) {
				sendToNuclos(callId, from, getNuclosAccountIfAny(from), service, to, getNuclosAccountIfAny(to), PhoneCallNotificationContext.State.START);
			}
			if (state == YourPhoneSystemState.IDLE) {
				// disconnected -> END
				mapRequestCall.remove(callId);
				sendToNuclos(callId, from, getNuclosAccountIfAny(from), service, to, getNuclosAccountIfAny(to), PhoneCallNotificationContext.State.END);
			}
		}
	}
	
	void simulateIncomingCall(String from, String service, String to) {
		String callId = "SimulatedCall_" + UUID.randomUUID().toString();
		try {
			sendToNuclos(callId, from, getNuclosAccountIfAny(from), service, to, getNuclosAccountIfAny(to), PhoneCallNotificationContext.State.RINGING);
			Thread.sleep(2000);
			sendToNuclos(callId, from, getNuclosAccountIfAny(from), service, to, getNuclosAccountIfAny(to), PhoneCallNotificationContext.State.START);
			Thread.sleep(2000);
			sendToNuclos(callId, from, getNuclosAccountIfAny(from), service, to, getNuclosAccountIfAny(to), PhoneCallNotificationContext.State.END);
		} catch (Exception ex) {
			LOG.warn(ex.getMessage(), ex);
		}
	}
	
	private void sendToNuclos(String id, String from, NuclosUserCommunicationAccount fromAccount, String service, String to, NuclosUserCommunicationAccount toAccount, PhoneCallNotificationContext.State notificationState) {
		if (mapRequestCall.containsKey(id)) {
			// send response also
			RequestedCall requestedCall = mapRequestCall.get(id);
			PhoneCallResponse.State state = null;
			switch (notificationState) {
			case RINGING: state = State.RINGING; break;
			case START: state = State.START; break;
			case END: state = State.END; break;
			}
			requestedCall.context.clearResponse().setState(state);
			try {
				CommunicationProvider.handleResponse(requestedCall.context);
			} catch (BusinessException e) {
				LOG.error("Sending response: " + requestedCall.context + " failed: + " + e.getMessage(), e);
			}
		}
		
		try {
			PhoneCallNotificationContext callContext = CommunicationProvider.newContextInstance(this, PhoneCallNotificationContext.class);
			callContext.setId(id);
			callContext.setFromNumber(from);
			if (fromAccount != null) {
				callContext.setFromUserId(fromAccount.getUserId());
			}
			callContext.setServiceNumber(service);
			callContext.setToNumber(to);
			if (toAccount != null) {
				callContext.setToUserId(toAccount.getUserId());
			}
			callContext.setState(notificationState);
			LOG.info("Sending event to nuclos: " + callContext);
			
			try {
				CommunicationProvider.handleNotification(callContext);
			} catch (BusinessException e) {
				LOG.error("Sending notification: " + callContext + " failed: + " + e.getMessage(), e);
			}
		} catch (BusinessException e1) {
			LOG.error("Creating context failed: + " + e1.getMessage(), e1);
		}
	}
	
	private NuclosUserCommunicationAccount getNuclosAccountIfAny(String agent) {
		if (mapAccounts.containsKey(agent)) {
			return mapAccounts.get(agent);
		}
		if (mapAccountsNull.containsKey(agent)) {
			return null;
		}
		
		Query<NuclosUserCommunicationAccount> q = QueryProvider.create(NuclosUserCommunicationAccount.class);
		q.where(NuclosUserCommunicationAccount.Account.eq(agent));
		q.and(NuclosUserCommunicationAccount.CommunicationPortId.eq(portId));
		try {
			NuclosUserCommunicationAccount nucAccount = QueryProvider.executeSingleResult(q);
			if (nucAccount != null) {
				mapAccounts.put(agent, nucAccount);
				LOG.info("Nuclos user account for agent '" + agent + "' found and cached!");
				return nucAccount;
			}
		} catch (BusinessException e) {
			// ignore
		}
		
		mapAccountsNull.put(agent, NULL);
		LOG.info("Nuclos user account for agent '" + agent + "' not found.");
		return null;
	}
	
	private static class RequestedCall {
		final Long requestedAt;
		final PhoneCallRequestContext context;
		
		public RequestedCall(PhoneCallRequestContext context) {
			this.context = context;
			this.requestedAt = System.currentTimeMillis() / 1000;
		}
	}

}
