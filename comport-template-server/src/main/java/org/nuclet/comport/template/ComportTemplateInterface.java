package org.nuclet.comport.template;

import org.nuclos.api.annotation.Communicator;
import org.nuclos.api.communication.CommunicationInterface;
import org.nuclos.api.context.communication.PhoneCallRequestContext;
import org.springframework.stereotype.Component;

@Component
@Communicator(connectionFactory = ComportFactory.class, domainName = "org.nuclet.comport.template", interfaceName = "comport-template", interfaceVersion = "1.0", manufacturer = "Novabit Informationssysteme GmbH")
public class ComportTemplateInterface implements CommunicationInterface {

	@Override
	public void register(Registration registration) {
		registration.setDescription("In Custom1 kann die Service Nummer angegeben werden, mit der ein Benutzer raustelefoniert.");
		registration.addSystemParameter("host", "IP der Telefonanlage");
		registration.addSystemParameter("port", "Default ist 2425");
		registration.addSystemParameter("simulateIncomingCallEveryXMin", "Parameter zum Simulieren eines eingehenden Anrufes");
		registration.addSystemParameter("simulateIncomingCallFromNumber", "Parameter zum Simulieren eines eingehenden Anrufes");
		registration.addSystemParameter("simulateIncomingCallToNumber", "Parameter zum Simulieren eines eingehenden Anrufes");
		registration.addSystemParameter("simulateIncomingCallServiceNumber", "Parameter zum Simulieren eines eingehenden Anrufes");
		registration.addSupportedRequestContext(PhoneCallRequestContext.class);
	}

}
