package org.nuclet.comport.template;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuclos.api.communication.CommunicationPortFactory;
import org.nuclos.api.exception.BusinessException;

public class ComportFactory implements CommunicationPortFactory<Comport> {
	
	private static Logger LOG = LogManager.getLogger(ComportFactory.class);
	
	private SimulateIncomingCallThread simulationThread = null;
	
	@Override
	public Comport newInstance(InstanceContext context) throws BusinessException {
		Comport port = new Comport();
		port.setId(context.getPortId());
		port.setHost(context.getSystemParameterValues().get("host"));
		port.setPort(context.getSystemParameterValues().get("port"));
		
		new Thread(port).start();
		
		String simulateIncomingCallEveryXMin = context.getSystemParameterValues().get("simulateIncomingCallEveryXMin");
		if (!StringUtils.isEmpty(simulateIncomingCallEveryXMin)) {
			stopSimulation();
			simulationThread = new SimulateIncomingCallThread();
			simulationThread.port = port;
			simulationThread.minsToWait = simulateIncomingCallEveryXMin;
			simulationThread.from = context.getSystemParameterValues().get("simulateIncomingCallFromNumber");
			simulationThread.service = context.getSystemParameterValues().get("simulateIncomingCallServiceNumber");
			simulationThread.to = context.getSystemParameterValues().get("simulateIncomingCallToNumber");
			simulationThread.start();
		}
		
		return port;
	}

	@Override
	public void shutdown(Comport port) throws BusinessException {
		stopSimulation();
		port.shutdown();
	}
	
	private void stopSimulation() {
		try {
			if (simulationThread == null) {
				return;
			}
			simulationThread.kill = true;
		} catch (Exception ex) {
			// igonore
		}
	}
	
	private class SimulateIncomingCallThread extends Thread {
		
		boolean kill = false;
		
		Comport port;
		
		String minsToWait = "1";
		String from;
		String service;
		String to;
		
		public SimulateIncomingCallThread() {
			super("Simulate incoming call for Comport");
		}
		
		@Override
		public void run() {
			while (true) {
				try {
					double sleep = Double.parseDouble(minsToWait);
					if (sleep <= 0) {
						break;
					}
					sleep((new Double(sleep * 60d * 1000d)).intValue());
					if (kill) {
						break;
					}
					port.simulateIncomingCall(from, service, to);
				} catch (Exception e) {
					LOG.warn(e.getMessage(), e);
					break;
				}
			}
		}
		
	}

}
