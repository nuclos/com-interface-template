#!/bin/bash

# for newly created self-signed key
KEYSTORE_DNAME="cn=Firstname Lastname, ou=Unit, o=Novabit, c=DE"
KEYSTORE_KEYALIAS="extension"
KEYSTORE_STOREPASSWD="nuclos"
KEYSTORE_KEYPASSWD="nuclos"

# YOU MUST use ' here - it will be 'lazy' evaluated later (tp)
# use timestamp service
SIGN_OPTIONS='-tsa http://tsa.starfieldtech.com/ -tsacert $KEYSTORE_KEYALIAS'
# don't use timestamp service
# SIGN_OPTIONS=''
# strip unknown attributes
PACK200_OPTIONS="-Ustrip"
# don't strip unknown attributes
#PACK200_OPTIONS=""

# Do not edit beneath this line 
#######################################
REGEX_DEP="^\s*(.+):(.+):(.+):(.+):(.+)$"
MAVEN_REPOSITORY="$HOME/.m2/repository"
CACHE="$HOME/jar.cache"

BIN_DIR=`readlink -f $(dirname $0)`
INSTALL_DIR=.
#INSTALL_DIR=$1
#if [ ! -d $INSTALL_DIR ]; then
#	echo "Usage: $0 <nuclos_install_dir>" 
#	exit -2
#fi
#shift

#if [ ! -d "$INSTALL_DIR/webapp/app" ]; then
#	echo "Unlikely that $INSTALL_DIR is a nuclos installation directory"
#	exit -2
#fi

EXTENSION_DIR=`pwd`
REGEX_IGNORE="(aspectjtools|nuclos-common-api|nuclos-client-api)-.*jar"
REGEX_IGNORE_EXTENSION=".*-(sources|javadoc).jar"
REGEX_IGNORE_SYNTH=".*synthetica.*"
# only cache jar that we don't build and are not SNAPSHOTs
CACHE_REGEX="(nuclos-|.*-common-.*|.*-client-.*|.*SNAPSHOT|all-in-one).*"

if [ -z "$JAVA_HOME" ]; then
	JARSIGNER=jarsigner
	JAR=jar
	PACK200=pack200
	KEYTOOL=keytool
elif [[ $JAVA_HOME =~ ^(.*)/jre(/)?$ ]]; then
	# http://stackoverflow.com/questions/1891797/capturing-groups-from-a-grep-regex
	JAVA_HOME=${BASH_REMATCH[1]}
	JARSIGNER=$JAVA_HOME/bin/jarsigner
	JAR=$JAVA_HOME/bin/jar
	PACK200=$JAVA_HOME/bin/pack200
	KEYTOOL=$JAVA_HOME/bin/keytool
else
	JARSIGNER=$JAVA_HOME/bin/jarsigner
	JAR=$JAVA_HOME/bin/jar
	PACK200=$JAVA_HOME/bin/pack200
	KEYTOOL=$JAVA_HOME/bin/keytool
fi
if [ -z "$SIGNER_JAVA_HOME" ]; then
	: # do nothing - user jar, jarsigner, pack200 from JAVA_HOME
else
	JARSIGNER=$SIGNER_JAVA_HOME/bin/jarsigner
	JAR=$SIGNER_JAVA_HOME/bin/jar
	PACK200=$SIGNER_JAVA_HOME/bin/pack200
	KEYTOOL=$SIGNER_JAVA_HOME/bin/keytool
	if [ ! -f "$JARSIGNER" ]; then
		echo "SIGNER_JAVA_HOME set to $SIGNER_JAVA_HOME, but jarsigner not found!"
		exit -1
	fi
fi
if [ -z "$TMPDIR" ]; then
	TMPDIR="/tmp"
fi
if [ ! -d "$TMPDIR" ]; then
	echo "TMPDIR set to $TMPDIR, but no directory"
	exit -1
fi
# BUILDDIR=`mktemp -d "$TMPDIR/prepare_deployXXXXXXX"`

mkAppLst() {
	local pom=`mvn help:effective-pom | grep 'nuclos.version>'`
	local regex=">(.*)<"
	local current=`pwd`
	if [[ "$pom" =~ $regex ]]; then
		local version="${BASH_REMATCH[1]}"
		echo $version
		local njar="$MAVEN_REPOSITORY/org/nuclos/nuclos-client/$version/nuclos-client-$version.jar"
		if [[ ! -f "$njar" ]]; then
			echo "Can't find $njar, aborting..."
			exit -1
		fi
		local dir=`mktemp -d "$TMPDIR/prepare_deployXXXXXXX"`
		pushd $dir
			unzip "$njar" "META-INF/nuclos/app.lst"
			cp "META-INF/nuclos/app.lst" "$BIN_DIR"
			if [[ "$?" != 0 ]]; then
				echo "no META-INF/nuclos/app.lst at $njar, aborting ..."
				exit -1
			fi
		popd
		rm -rf $dir
	else 
		echo "Can't determine nuclos version the extension depends on at $current"
		exit -1
	fi
}

copyDeps() {
	local DIR="$1"
	shift
	local COPY_DIR="$1"
	shift
	pushd "$DIR"
		rm -f "list.txt"
		# https://maven.apache.org/plugins/maven-dependency-plugin/list-mojo.html
		mvn dependency:list -DincludeScope=runtime -DoutputFile=list.txt >/dev/null
		if [[ "$?" != 0 ]]; then
			echo "mvn dependency:list failed; unable to proceed"
			mvn dependency:list -DincludeScope=runtime
			exit -1
		fi
		mkAppLst
	popd
	for i in `cat $DIR/list.txt`; do
		if [[ "$i" =~ $REGEX_DEP ]]; then
			echo "$i"
			local GROUPID="${BASH_REMATCH[1]}"
			local ARTEFACTID="${BASH_REMATCH[2]}"
			local TYPE="${BASH_REMATCH[3]}"
			local VERSION="${BASH_REMATCH[4]}"
			local SCOPE="${BASH_REMATCH[5]}"
			local PTH=`echo "$GROUPID" | /bin/sed -e 's/\./\//g'`
			local FILE_DEP="$MAVEN_REPOSITORY/$PTH/$ARTEFACTID/$VERSION/$ARTEFACTID-$VERSION.jar"
			# echo "$GROUPID -> $PATH"
			# echo "$FILE_DEP"
			# ls "$FILE_DEP"
			if [[ ! -f "$FILE_DEP" ]]; then
				echo "Can't find $FILE_DEP, aborting..."
				exit -1
			fi
			
			if [[ "$i" =~ $REGEX_IGNORE ]]; then
				continue;
			fi
			if [ ! -f "$BIN_DIR/app.lst" ]; then
				local current=`pwd`
				echo "no app.lst at $current"
			fi
			if grep "$i" $BIN_DIR/app.lst; then
				# do nothing - this is a dependency of nuclos
				echo "$i is a nuclos dependency"
			else 
				cp "$FILE_DEP" "$COPY_DIR"
			fi
		fi
	done
	rm -f "$DIR/list.txt"
}

processDeps() {
	local DIR="$1"
	shift
	local COPY_DIR="$1"
	shift
	local TARGET_DIR="$1"
	shift
	copyDeps "$DIR" "$COPY_DIR"
	pushd "$COPY_DIR"
		for i in *.jar; do
			if [[ "$i" =~ $REGEX_IGNORE  || "$i" == "*.jar" ]]; then
				continue;
			fi
			#if [[ ! ( -f "$INSTALL_DIR/webapp/app/$i" || -f "$INSTALL_DIR/webapp/app/$i.pack.gz" ) ]]; then
			if grep "$i" $BIN_DIR/app.lst; then
				# do nothing - this is a dependency of nuclos
				echo "$i is a nuclos dependency"
			else 
				local CACHED="$CACHE/$i"
				if [[ -f "$CACHED" && ! "$i" =~ $CACHE_REGEX ]]; then
					echo "Using cached $CACHED"
					cp "$CACHED" "$i"
				else 
					unsignAndFixManifest "$i"
					sign "$i"
					if [[ ! "$i" =~ $CACHE_REGEX ]]; then
						echo "copying $i to cache"
						cp "$i" "$CACHE"
					fi
				fi
				echo "cp $i ../$TARGET_DIR"
				cp "$i" "../$TARGET_DIR"
			fi
		done
	popd
}

unsign() {
        local JAR=`readlink -f "$1"`
        shift
        zip -q -d "$JAR" META-INF/\*.SF META-INF/\*.DSA META-INF/\*.RSA META-INF/INDEX.LIST >>/dev/null
        # META-INF/MANIFEST.MF
	$PACK200 "$PACK200_OPTIONS" -r "$JAR" "$JAR" 2>/dev/null
	if [[ "$?" != 0 ]]; then
		echo "repacking failed; unable to proceed"
		$PACK200 "$PACK200_OPTIONS" -r "$JAR" "$JAR"
		exit -1
	fi
}

unsignAndFixManifest() {
        local JAR=`readlink -f "$1"`
        shift
        zip -q -d "$JAR" META-INF/\*.SF META-INF/\*.DSA META-INF/\*.RSA META-INF/INDEX.LIST >>/dev/null
        # META-INF/MANIFEST.MF
	jar umf "$MANIFEST" "$JAR"
	$PACK200 $PACK200_OPTIONS -r "$JAR" "$JAR" 2>/dev/null
	if [[ "$?" != 0 ]]; then
		echo "repacking failed; unable to proceed"
		$PACK200 $PACK200_OPTIONS -r "$JAR" "$JAR"
		exit -1
	fi
}

sign() {
	local JAR="$1"
	shift
	$JARSIGNER $SIGN_OPTIONS -keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
		-keypass "$KEYSTORE_KEYPASSWD" "$JAR" "$KEYSTORE_KEYALIAS" >>/dev/null 
	if [[ "$?" != 0 ]]; then
		echo "signing failed; unable to proceed"
		$JARSIGNER $SIGN_OPTIONS -keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
			-keypass "$KEYSTORE_KEYPASSWD" "$JAR" "$KEYSTORE_KEYALIAS"
		exit -1
	fi
}

addJarToJar() {
	local TARGET=`readlink -f "$1"`
	shift
	local ADD="$1"
	shift
	local TMP_DIR1=`mktemp -d "$TMPDIR/prepare_deployXXXXXXX"`
	echo "addJarToJar $TARGET $ADD"
	unzip -q "$ADD" -d "$TMP_DIR1"
	pushd "$TMP_DIR1"
		rm -f META-INF/\*.SF META-INF/\*.DSA META-INF/\*.RSA META-INF/INDEX.LIST
		zip -u9rq "$TARGET" . 
	popd
	rm -rf "$TMP_DIR1"
}

cpsigned() {
	local FROM="$1"
	shift
	local TO="$1"
	for i in $FROM; do
		if [[ "$i" =~ $REGEX_IGNORE_EXTENSION ]]; then
			continue;
		fi
		cp "$i" "$TO"
	done
}

KEYSTORE_FILE="$EXTENSION_DIR/key"
# Check for 'official' key store
if [[ -n "$KEYSTORE" ]]; then
	KEYSTORE_KEYALIAS="$ALIAS"
	KEYSTORE_STOREPASSWD="$STOREPASS"
	KEYSTORE_KEYPASSWD="$KEYPASS"
	cp "$KEYSTORE" "$KEYSTORE_FILE"
fi
# Create keystore if needed
if [ ! -f "$KEYSTORE_FILE" ]; then
	$KEYTOOL -genkeypair -dname "$KEYSTORE_DNAME" \
		-alias "$KEYSTORE_KEYALIAS" -keypass "$KEYSTORE_KEYPASSWD" \
		-keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
		-validity 180 || exit -2
fi

# Make a manifest template
MANIFEST=`mktemp "$TMPDIR/prepare_deployXXXXXXX"`
cat <<-HERE >$MANIFEST
Application-Name: nuclos.de ERP
Trusted-Library: true
Trusted-Only: true
Codebase: *
Permissions: all-permissions
HERE

mkdir -p "$CACHE"
if [ ! -d "$CACHE" ]; then
	echo "Can't create/access jar cache at $CACHE"
	exit -1
fi

# http://wiki.nuclos.de/display/Konfiguration/Entwicklungsumgebung
pushd "$INSTALL_DIR"
	rm -rf "extensions"
	mkdir -p "extensions/common/native"
	mkdir -p "extensions/client/themes"
	mkdir -p "extensions/server"
popd

# lazy evaluation
SIGN_OPTIONS=$(eval echo $SIGN_OPTIONS)

COMMON_DIR=`ls -d *-common`
CLIENT_DIR=`ls -d *-client`
THEME_DIR=`ls -d *-theme`
SERVER_DIR=`ls -d *-server`
if [ -n "$COMMON_DIR" ]; then
	pushd "$COMMON_DIR/target"
		JAR=`ls -1 *-common-*.jar`
		for i in $JAR; do
			if [[ "$i" =~ $REGEX_IGNORE_EXTENSION ]]; then
				continue;
			fi
			unsign "$i"
			sign "$i"
		done
	popd
fi
if [ -n "$CLIENT_DIR" ]; then
	pushd "$CLIENT_DIR/target"
		JAR=`ls -1 *-client-*.jar`
		for i in $JAR; do
			if [[ "$i" =~ $REGEX_IGNORE_EXTENSION ]]; then
				continue;
			fi
			unsign "$i"
			sign "$i"
		done
	popd
fi
if [ -n "$THEME_DIR" ]; then
	pushd "$THEME_DIR"
		# https://maven.apache.org/plugins/maven-dependency-plugin/list-mojo.html
		mvn dependency:list -DincludeScope=runtime -DoutputFile=list.txt >/dev/null
		if [[ "$?" != 0 ]]; then
			echo "mvn dependency:list failed; unable to proceed"
			mvn dependency:list -DincludeScope=runtime
			exit -1
		fi
	popd
	pushd "$THEME_DIR/target"
		JAR=`ls -1 *-theme-*.jar`
		for i in $JAR; do
			if [[ "$i" =~ $REGEX_IGNORE_EXTENSION ]]; then
				continue;
			fi
			unsign "$i"
			
			# special treatment for theme - see http://wiki.nuclos.de/pages/viewpage.action?pageId=823180
			for j in `cat ../list.txt`; do
				if [[ ! "$j" =~ $REGEX_IGNORE_SYNTH ]]; then
					continue;
				fi
				if [[ "$j" =~ $REGEX_DEP ]]; then
					echo "$j"
					GROUPID="${BASH_REMATCH[1]}"
					ARTEFACTID="${BASH_REMATCH[2]}"
					TYPE="${BASH_REMATCH[3]}"
					VERSION="${BASH_REMATCH[4]}"
					SCOPE="${BASH_REMATCH[5]}"
					PTH=`echo "$GROUPID" | /bin/sed -e 's/\./\//g'`
					FILE_DEP="$MAVEN_REPOSITORY/$PTH/$ARTEFACTID/$VERSION/$ARTEFACTID-$VERSION.jar"
					if [[ ! -f "$FILE_DEP" ]]; then
						echo "Can't find $FILE_DEP, aborting..."
						exit -1
					fi
					# echo "$GROUPID -> $PATH"
					# echo "$FILE_DEP"
					# ls "$FILE_DEP"
					addJarToJar "$i" "$FILE_DEP"
				fi
			done
			rm -f ../list.txt
			
			sign "$i"
		done
	popd
fi

# glob must be deferred (tp)
cpsigned '*-common/target/*-common-*.jar' "$INSTALL_DIR/extensions/common"
cpsigned '*-client/target/*-client-*.jar' "$INSTALL_DIR/extensions/client"
cpsigned '*-theme*/target/*-theme*-*.jar' "$INSTALL_DIR/extensions/client/themes"
cpsigned '*-server/target/*-server-*.jar' "$INSTALL_DIR/extensions/server"

rm -rf deps
mkdir deps
if [ -n "$COMMON_DIR" ]; then
	processDeps "$COMMON_DIR" deps "$INSTALL_DIR/extensions/common"
fi

rm -rf deps
mkdir deps
if [ -n "$SERVER_DIR" ]; then
	processDeps $SERVER_DIR deps "$INSTALL_DIR/extensions/server"
fi

rm -rf deps
mkdir deps
if [ -n "$CLIENT_DIR" ]; then
	processDeps $CLIENT_DIR deps "$INSTALL_DIR/extensions/client"
fi
rm -f $INSTALL_DIR/extensions/client/*-common-*.jar

rm -rf deps "$MANIFEST"
rm -rf "$BIN_DIR/app.lst"

tree "$INSTALL_DIR/extensions"